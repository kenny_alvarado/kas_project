# README #

### What is this repository for? ###

Test for ALTRAN from Kenneth Alvarado Perez

### How do I get set up? ###

To compile and execute the project you must move to the folder with the file pom.xml and use these commands:

mvn clean install

mvn spring-boot:run

### End points ###

To consult the organizations the endpoint available is

http://localhost:8080/organization?language=en&rows=10&start=4

http://localhost:8080/actuator/

http://localhost:8080/actuator/health

### Answers ###
The answers for the questions that are after to the test are in the file answer_test