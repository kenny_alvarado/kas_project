package com.altran.kas.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.altran.kas.dto.model.ResultDto;
import com.altran.kas.model.SearchResult;
import com.altran.kas.requester.OrganizationRequester;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class OrganizationServiceTest {

	private OrganizationRequester organizationRequester = Mockito.mock(OrganizationRequester.class);
	
	private OrganizationService organizationService;
	
	private static final String RESULT_RESPONSE_TEST_FILE = "src/test/resources/result.json";
	
	private SearchResult searchResult; 
	
	@Before
	public void init() throws JsonParseException, JsonMappingException, IOException {
		organizationService = new OrganizationService(organizationRequester);
		File file = new File(RESULT_RESPONSE_TEST_FILE);
		ObjectMapper objectMapper = new ObjectMapper();
		searchResult = objectMapper.readValue(file, SearchResult.class);
	}
	
	@Test
	public void testGetOrganizationURLCorrectLanguage() {
		String urlSpanish = "https://www.bcn.cat/estadistica/castella/dades/timm/classol/locals/index.htm";
		when(organizationRequester.getOrganizations(anyInt() ,anyInt())).thenReturn(searchResult);
		ResultDto organizationDTO = organizationService.getOrganizations("es", 0, 10);
		
		assertTrue(urlSpanish.equals(organizationDTO.getContent().get(0).getUrl()));
	}
	
	@Test
	public void testGetOrganizationDescriptionCorrectLanguage() {
		String organizationDescriptionEnglish = "Housing";
		when(organizationRequester.getOrganizations(anyInt() ,anyInt())).thenReturn(searchResult);
		ResultDto organizationDTO = organizationService.getOrganizations("en", 0, 10);
		
		assertTrue(organizationDescriptionEnglish.equals(organizationDTO.getContent().get(0).getDescription()));
	}
	
	@Test
	public void testGetOrganizationGetCodeHasValue() {
		when(organizationRequester.getOrganizations(anyInt() ,anyInt())).thenReturn(searchResult);
		ResultDto organizationDTO = organizationService.getOrganizations("es", 0, 10);
		assertNotNull(organizationDTO.getContent().get(0).getCode());
	}
}
