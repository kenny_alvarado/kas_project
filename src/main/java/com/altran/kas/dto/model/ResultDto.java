package com.altran.kas.dto.model;

import com.altran.kas.model.Result;

public class ResultDto extends Result<OrganizationDto>{

	private Integer count;
	private Integer start;
	private Integer rows;

	public ResultDto(Integer count, Integer start, Integer rows) {
		super();
		this.count = count;
		this.start = start;
		this.rows = rows;
		//super.content = content;
	}	

	public Integer getCount() {
		return count;	
	}
	
	public void setCount(Integer count) {
		this.count = count;
	}

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public Integer getRows() {
		return rows;
	}

	public void setRows(Integer rows) {
		this.rows = rows;
	}

}
