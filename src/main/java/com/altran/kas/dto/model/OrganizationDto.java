package com.altran.kas.dto.model;

import com.altran.kas.model.Organization;

public class OrganizationDto {

	String url;
	String code;
	String description;
	
	public OrganizationDto(String language, Organization organization) {
		this.code = organization.getCode();
		this.description = organization.getTranslatedDescription().get(language);
		this.url = organization.getTranslatedURL().get(language);
	}

	public String getUrl() {
		return url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
}
