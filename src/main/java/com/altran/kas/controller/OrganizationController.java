package com.altran.kas.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.altran.kas.dto.model.ResultDto;
import com.altran.kas.request.model.OrganizationRequest;
import com.altran.kas.service.OrganizationService;
import com.altran.kas.util.GeneralConstants;

@RestController
@RequestMapping(value = "/organization", produces = GeneralConstants.PRODUCES_JSON_MEDIA_TYPE)
public class OrganizationController {
	
	OrganizationService organizationService;
	
	public OrganizationController(OrganizationService organizationService) {
		this.organizationService = organizationService;
	}


	@RequestMapping(value = "", method = RequestMethod.GET)
	public ResultDto getOrganization(HttpServletRequest httpRequestm,OrganizationRequest request) {
		return organizationService.getOrganizations(request);
	}
}
