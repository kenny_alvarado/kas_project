package com.altran.kas.util;

public class GeneralConstants {

	public static final String PRODUCES_JSON_MEDIA_TYPE = "application/json";
	public static final String ROWS = "rows";
	public static final String START = "start";
	public static final Integer DEFAULT_ROWS = 10;
	
}
