package com.altran.kas.service;

import org.springframework.beans.factory.annotation.Value;

import com.altran.kas.dto.model.ResultDto;
import com.altran.kas.model.SearchResult;
import com.altran.kas.request.model.BaseRequest;
import com.altran.kas.util.GeneralConstants;

public class BaseService {

	@Value( "${default.language}" )
	private String defaultLanguage;
	
	public void setQueryValues(BaseRequest baseRequest) {
		
		if(baseRequest.getLanguage() == null) {
			baseRequest.setLanguage(defaultLanguage);
		}
		
		if(baseRequest.getStart() == null) {
			baseRequest.setStart(0);
		}
		
		if(baseRequest.getRows() == null) {
			baseRequest.setRows(GeneralConstants.DEFAULT_ROWS); 
		}
	}
	
	public <T> ResultDto getPagination(SearchResult searchResult, Integer start, Integer rows) {
		
		ResultDto resultDto = new ResultDto(searchResult.getResult().getCount(), start, rows);
		
		return resultDto;
	}
	
}
