package com.altran.kas.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.altran.kas.dto.model.OrganizationDto;
import com.altran.kas.dto.model.ResultDto;
import com.altran.kas.model.Organization;
import com.altran.kas.model.SearchResult;
import com.altran.kas.request.model.OrganizationRequest;
import com.altran.kas.requester.OrganizationRequester;

@Service
public class OrganizationService extends BaseService{

	OrganizationRequester organizationRequester;
	
	public OrganizationService(OrganizationRequester organizationRequester) {
		super();
		this.organizationRequester = organizationRequester;
	}

	public ResultDto getOrganizations(OrganizationRequest organizationRequest) {
		this.setQueryValues(organizationRequest);
		return getOrganizations(organizationRequest.getLanguage(), organizationRequest.getRows(), 
				organizationRequest.getStart());
	}

	public ResultDto getOrganizations(String languange, Integer rows, Integer start) {
		List<OrganizationDto> organizationDtos = new ArrayList<OrganizationDto>();
		
		SearchResult searchResult = organizationRequester.getOrganizations(rows, start);	
		List<Organization> organizations  = searchResult.getResult().getContent(); 
		organizations.forEach(organization ->{
			OrganizationDto organizationDto = new OrganizationDto(languange, organization);
			organizationDtos.add(organizationDto);
		});
		
		ResultDto resultDto = this.getPagination(searchResult, start, rows);
		resultDto.setContent(organizationDtos);
		
		return resultDto;
	}
	
}
