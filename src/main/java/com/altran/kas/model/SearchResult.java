package com.altran.kas.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchResult {

	private String success;
	Result<Organization> result;
	
	public String getSuccess() {
		return success;
	}
	
	public void setSuccess(String success) {
		this.success = success;
	}
	
	public Result<Organization> getResult() {
		return result;
	}
	
	public void setResult(Result<Organization> result) {
		this.result = result;
	}	
	
}
