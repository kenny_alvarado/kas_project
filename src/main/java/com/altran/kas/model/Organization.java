package com.altran.kas.model;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Organization {
	
	String code;
		
	@JsonProperty("url_tornada")
	Map<String, String> translatedURL;
	
	Map<String, String> translatedDescription;
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}

	public Map<String, String> getTranslatedDescription() {
		return translatedDescription;
	}

	public void setTranslatedDescription(Map<String, String> translatedDescription) {
		this.translatedDescription = translatedDescription;
	}

	public Map<String, String> getTranslatedURL() {
		return translatedURL;
	}

	public void setTranslatedURL(Map<String, String> translatedURL) {
		this.translatedURL = translatedURL;
	}
	
	@SuppressWarnings("unchecked")
	@JsonProperty("organization")
	private void unpackNested(Map<String,Object> organization) {
		Map<String,String> translatedDescription = (Map<String,String>)organization.get("description_translated");
		this.translatedDescription = translatedDescription;
	}
	
}
