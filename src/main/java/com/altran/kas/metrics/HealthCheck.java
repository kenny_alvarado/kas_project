package com.altran.kas.metrics;

import org.springframework.boot.actuate.health.AbstractHealthIndicator;
import org.springframework.boot.actuate.health.Health.Builder;
import org.springframework.stereotype.Component;

@Component
public class HealthCheck extends AbstractHealthIndicator  {
  
	  @Override
	  protected void doHealthCheck(Builder builder) throws Exception {
	    builder
	        .up()
	        .withDetail("details", "Application test for ALTRAN");
	  }
}
