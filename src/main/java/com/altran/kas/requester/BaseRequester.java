package com.altran.kas.requester;

import java.util.Map;
import java.util.Map.Entry;

import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

public class BaseRequester<T> {
	
	RestTemplate restTemplate;
	
	public BaseRequester(RestTemplate restTemplate) {
		super();
		this.restTemplate = restTemplate;
	}

	public T getRequest(String url, Class<T> type, Map<String, Object> requestParameters) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url);
		setURLParameters(builder, requestParameters);
		return restTemplate.getForObject(builder.toUriString(), type);
	}
	
	private void setURLParameters(UriComponentsBuilder builder, Map<String, Object> requestParameters) {
		for(Entry<String, Object> pair : requestParameters.entrySet()) {
			builder.queryParam(pair.getKey(), pair.getValue());
		}
	}

}
