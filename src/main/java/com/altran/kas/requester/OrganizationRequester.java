package com.altran.kas.requester;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import com.altran.kas.model.SearchResult;
import com.altran.kas.util.GeneralConstants;

@Repository
public class OrganizationRequester extends BaseRequester<SearchResult> {
	
	@Value( "${url.opendata-ajuntament.search}" )
	private String organizationExternalUrl;
	
	public OrganizationRequester(RestTemplate restTemplate) {
		super(restTemplate);
	}

	@Cacheable("oganizations")
	public SearchResult getOrganizations(Integer rows, Integer start) {
		
		Map<String, Object> requestParameters = new HashMap<String, Object>();
		requestParameters.put(GeneralConstants.ROWS, rows);
		requestParameters.put(GeneralConstants.START, start);
		
		SearchResult result =  this.getRequest(organizationExternalUrl, SearchResult.class, requestParameters);
		return result;
	}

}
